# NGINX

## CONFIGURABLE BY ENV VARIABLES

STREAM_HOST
STREAM_PORT
STREAM_STATICS_PATH

## Templates availabled:

| Nom du template | Description|
 |:-----------|:---|
 |[php-fpm-with-statics.template](templates/php-fpm-with-statics.template)| Configuration vhost de base vers une source php `fastcgi_pass  STREAM_HOST:STREAM_PORT` et un path vers un ou des elements statiques `STREAM_STATICS_PATH`  |
 |[plateforme-front.template](templates/plateforme-front.template)| Configuration vhost specifique pour le front de la plateforme prospects. Configuration vhost de base vers une source php `fastcgi_pass  STREAM_HOST:STREAM_PORT` , un path vers un ou des elements statiques `STREAM_STATICS_PATH` et une url `STREAM_API_HOST` vers le service api distant via une configuration proxy_pass `proxy_pass http://${STREAM_API_HOST}/api/register;`|

#HOW TO USE IN DOCKER-COMPOSE.YML
```yaml
back-httpd:
  image: registry.gitlab.com/betd/public/docker/nginxnginx:1.21-env-conf
  container_name: back_httpd
  environment:
    STREAM_HOST: php
    STREAM_PORT: 9000
    STREAM_STATICS_PATH: upload|assets|manifest.json
  command: /bin/sh -c "envsubst '$$STREAM_HOST $$STREAM_PORT $$STREAM_STATICS_PATH' < /etc/nginx/conf.d/php-fpm-with-statics.template > /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"
  depends_on:
    - back_php
  links:
    - back_php:php
```




