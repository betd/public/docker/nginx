FROM nginx:1.21-alpine

LABEL maintenainer="Pierre Pottié <pierre.pottie@gmail.com>"
ARG TZ=Europe/Paris
ENV TZ=${TZ}
RUN apk add  --update --no-cache tzdata  && \
    echo "${TZ}" >  /etc/timezone && \
    cp /usr/share/zoneinfo/${TZ} /etc/localtime &&\
    apk del tzdata
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./templates/* /etc/nginx/conf.d/

RUN apk update && apk upgrade --no-cache --prune -f && rm -rf /var/cache/apk/*